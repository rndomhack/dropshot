/* global enchant, Class, Core, Scene, Group, Sprite, Surface, Timeline, ds*/

"use strict";

{
    enchant();

    let core;

    const DsMain = Class.create(Scene, {
        initialize(options) {
            Scene.call(this);

            this.backgroundColor = "#dddddd";

            this.currentLife = options.life;
            this.currentTime = 0;
            this.currentScore = 0;
            this.currentPlayer = null;
            this.players = [];
            this.enemies = [];
            this.effects = [];
            this.points = [];
            this.circles = [];
            this.time = options.time;
            this.life = options.life;
            this.table = options.table;

            this.layers = {};

            this.layers.field = new Group();
            this.addChild(this.layers.field);

            this.layers.effect = new Group();
            this.addChild(this.layers.effect);

            this.layers.state = new Group();
            this.addChild(this.layers.state);

            this.pauseButton = new DsButton({
                width: 24,
                height: 24,
                x: 616,
                y: 0,
                image: core.assets[ds.images.icon_pause],
                ontouchend: () => {
                    const tmp = new Surface(this.width / 2, this.height / 2);
                    const surface = new Surface(this.width, this.height);

                    tmp.context.drawImage(this._layers.Canvas._element, 0, 0, this.width / 2, this.height / 2);
                    surface.context.drawImage(tmp._element, 0, 0, this.width, this.height);

                    const pauseScene = new DsMainPause({
                        main: this,
                        image: surface
                    });

                    core.pushScene(pauseScene);
                }
            });
            this.layers.state.addChild(this.pauseButton);

            this.lifeLabel = new DsLabel({
                x: 0,
                y: 0,
                text: this.currentLife,
                image: core.assets[ds.images.icon_life]
            });
            this.layers.state.addChild(this.lifeLabel);

            this.timeLabel = new DsLabel({
                x: 96,
                y: 0,
                text: Math.floor((this.time - this.currentTime) / 60),
                image: core.assets[ds.images.icon_time]
            });
            this.layers.state.addChild(this.timeLabel);

            this.scoreLabel = new DsLabel({
                x: 96 * 2,
                y: 0,
                text: 0,
                image: core.assets[ds.images.icon_score]
            });
            this.layers.state.addChild(this.scoreLabel);

            this._element.addEventListener("mouseenter", () => {
                this.ontouchenter();
            });

            this._element.addEventListener("mouseleave", () => {
                this.ontouchleave();
            });
        },
        setLife(life) {
            if (life < 0) {
                this.currentLife = Math.max(this.currentLife + life, 0);
            } else {
                this.currentLife = life;
            }

            this.lifeLabel.text = this.currentLife;
        },
        getLife() {
            return this.currentLife;
        },
        setScore(score) {
            if (score < 0) {
                this.currentScore -= score;
            } else {
                this.currentScore = score;
            }

            this.scoreLabel.text = this.currentScore;
        },
        getScore() {
            return this.currentScore;
        },
        createPlayer(options) {
            return new DsPlayer(options);
        },
        createEnemy(options) {
            switch (options.type) {
                default:
                    return new DsEnemyNormal(options);
            }
        },
        getCurrentPlayer() {
            return this.currentPlayer;
        },
        setCurrentPlayer(player) {
            this.currentPlayer = player;
        },
        addPlayer(player) {
            this.layers.field.addChild(player);
            this.players.push(player);
        },
        addEnemy(enemy) {
            this.layers.field.addChild(enemy);
            this.enemies.push(enemy);
        },
        addEffect(effect) {
            this.layers.field.addChild(effect);
            this.effects.push(effect);
        },
        addPoint(point) {
            this.layers.effect.addChild(point);
            this.points.push(point);
        },
        addCircle(circle) {
            this.layers.effect.addChild(circle);
            this.circles.push(circle);
        },
        removePlayer(player) {
            this.layers.field.removeChild(player);
            this.players.splice(this.players.indexOf(player), 1);
        },
        removeEnemy(enemy) {
            this.layers.field.removeChild(enemy);
            this.enemies.splice(this.enemies.indexOf(enemy), 1);
        },
        removeEffect(effect) {
            this.layers.field.removeChild(effect);
            this.effects.splice(this.effects.indexOf(effect), 1);
        },
        removePoint(point) {
            this.layers.effect.removeChild(point);
            this.points.splice(this.points.indexOf(point), 1);
        },
        removeCircle(circle) {
            this.layers.effect.removeChild(circle);
            this.circles.splice(this.circles.indexOf(circle), 1);
        },
        onenterframe() {
            this.timeLabel.text = Math.ceil((this.time - this.currentTime) / 60);

            if (this.currentTime === this.time) {
                //console.log("time");
            }

            if (this.currentLife === 0) {
                //console.log("life");
            }

            if (this.table.hasOwnProperty(this.currentTime)) {
                const table = this.table[this.currentTime];

                if (table.hasOwnProperty("players")) {
                    for (const options of table.players) {
                        const player = this.createPlayer(options);
                        this.addPlayer(player);
                    }
                }

                if (table.hasOwnProperty("enemies")) {
                    for (const options of table.enemies) {
                        const enemy = this.createEnemy(options);
                        this.addEnemy(enemy);
                    }
                }
            }

            this.currentTime++;
        },
        ontouchstart(evt) {
            if (this.currentPlayer !== null) {
                this.currentPlayer.onparenttouchstart(evt);
            }
        },
        ontouchmove(evt) {
            if (this.currentPlayer !== null) {
                this.currentPlayer.onparenttouchmove(evt);
            }
        },
        ontouchend(evt) {
            if (this.currentPlayer !== null) {
                this.currentPlayer.onparenttouchend(evt);
            }
        },
        ontouchenter() {
            if (this.currentPlayer !== null) {
                this.currentPlayer.onparenttouchenter();
            }
        },
        ontouchleave() {
            if (this.currentPlayer !== null) {
                this.currentPlayer.onparenttouchleave();
            }
        }
    });

    const DsMainPause = Class.create(Scene, {
        initialize(options) {
            Scene.call(this);

            this.backgroundColor = "#dddddd";

            this.main = options.main;

            this.bg = new Sprite(this.width, this.height);
            this.bg.image = options.image;
            this.addChild(this.bg);

            this.bg2 = new Sprite(this.width, this.height);
            this.bg2.opacity = 0.4;
            this.bg2.image = new Surface(this.width, this.height);
            this.bg2.image.context.fillStyle = "#0000000";
            this.bg2.image.context.fillRect(0, 0, this.width, this.height);
            this.addChild(this.bg2);
        },
        ontouchend() {
            core.popScene().remove();
        }
    });

    const DsPlayer = Class.create(Sprite, {
        initialize(options) {
            Sprite.call(this, 48, 48);

            this.x = options.x;
            this.y = options.y;
            this.scaleX = 0.5;
            this.scaleY = 0.5;
            this.opacity = 0;

            this.endX = options.endX || options.x;
            this.endY = options.endY || options.y;
            this.origX = 0;
            this.origY = 0;
            this.startX = 0;
            this.startY = 0;
            this.moveX = 0;
            this.moveY = 0;
            this.speedX = 0;
            this.speedY = 0;
            this.ease = options.ease || "LINEAR";

            this.color = options.color;
            this.phase = "wait";
            this.time = options.time;
            this.point = null;
            this.score = 100;

            this.image = new Surface(48, 48);
            this.image.draw(core.assets[ds.images.color], -this.color * 48, 0);
            this.image.draw(core.assets[ds.images.player], 0, 0, 48, 48);

            this.tl
                .fadeIn(10).and().scaleTo(1, 10).and().delay(this.time - 60)
                .scaleTo(0.8, 10)
                .delay(50)
                .then(() => this.delete());

            this.tl2 = new Timeline(this)
                .moveTo(this.endX, this.endY, this.time - 60, enchant.Easing[this.ease]);
        },
        createEffect(pattern) {
            return new DsEffect({
                width: this.width,
                height: this.height,
                x: this.x,
                y: this.y,
                scaleX: this.scaleX,
                scaleY: this.scaleY,
                image: this.image,
                pattern: pattern
            });
        },
        createPoint() {
            return new DsPoint({
                x: this.x,
                y: this.y
            });
        },
        createCircle(chain) {
            return new DsCircle({
                x: this.x + this.width / 2 - 48,
                y: this.y + this.height / 2 - 48,
                color: this.color,
                chain: chain
            });
        },
        delete() {
            if (this.phase === "end") return;

            const effect = this.createEffect("delete");
            this.scene.addEffect(effect);

            if (this.point !== null) {
                this.point.delete();
                this.point = null;
            }

            if (this.scene.getCurrentPlayer() === this) {
                this.scene.setCurrentPlayer(null);
            }

            this.scene.removePlayer(this);

            this.phase = "end";
        },
        chain(chain) {
            if (this.phase === "end") return;

            this.scene.setScore(-this.score * ((1 + chain) / 2));

            const effect = this.createEffect("chain");
            this.scene.addEffect(effect);

            if (this.point !== null) {
                this.point.delete();
                this.point = null;
            }

            const circle = this.createCircle(chain);
            this.scene.addCircle(circle);

            if (this.scene.getCurrentPlayer() === this) {
                this.scene.setCurrentPlayer(null);
            }

            this.scene.removePlayer(this);

            this.phase = "end";
        },
        onenterframe() {
            if (this.phase === "end") return;

            if (this.speedX !== 0 || this.speedY !== 0) {
                this.x += this.speedX;
                this.y += this.speedY;
                this.speedX *= 0.9;
                this.speedY *= 0.9;

                const width = this.scene.width - this.width * this.scaleX;
                const height = this.scene.height - this.height * this.scaleY;

                if (this.x < 0) {
                    this.x = -this.x;
                    this.speedX = -this.speedX;
                } else if (this.x >= width) {
                    this.x = width - (this.x - width);
                    this.speedX = -this.speedX;
                }

                if (this.y < 0) {
                    this.y = -this.y;
                    this.speedY = -this.speedY;
                } else if (this.y >= height) {
                    this.y = height - (this.y - height);
                    this.speedY = -this.speedY;
                }

                if (Math.abs(this.speedX) < 0.01 && Math.abs(this.speedY) < 0.01) {
                    this.speedX = 0;
                    this.speedY = 0;
                }

                for (const player of this.scene.players) {
                    if (player === this) continue;
                    if (!this.intersectStrict(player)) continue;
                    if ((this.color !== 7 && player.color !== 7) && this.color !== player.color) continue;

                    const thisWidth = this.width * this.scaleX;
                    const thisHeight = this.height * this.scaleY;
                    const playerWidth = player.width * player.scaleX;
                    const playerHeight = player.height * player.scaleY;

                    const x = (this.x + thisWidth / 2) - (player.x + playerWidth / 2);
                    const y = (this.y + thisHeight / 2) - (player.y + playerHeight / 2);

                    if (Math.abs(x) > Math.abs(y)) {
                        if (x > 0) {
                            this.x = player.x + playerWidth;
                        } else {
                            this.x = player.x - thisWidth;
                        }
                    } else {
                        if (y > 0) {
                            this.y = player.y + playerHeight;
                        } else {
                            this.y = player.y - thisHeight;
                        }
                    }

                    player.chain(1);
                    this.chain(1);
                    return;
                }

                for (const enemy of this.scene.enemies) {
                    if (!this.intersectStrict(enemy)) continue;
                    if ((this.color !== 7 && enemy.color !== 7) && this.color !== enemy.color) continue;

                    const thisWidth = this.width * this.scaleX;
                    const thisHeight = this.height * this.scaleY;
                    const enemyWidth = enemy.width * enemy.scaleX;
                    const enemyHeight = enemy.height * enemy.scaleY;

                    const x = (this.x + thisWidth / 2) - (enemy.x + enemyWidth / 2);
                    const y = (this.y + thisHeight / 2) - (enemy.y + enemyHeight / 2);

                    if (Math.abs(x) > Math.abs(y)) {
                        if (x > 0) {
                            this.x = enemy.x + enemyWidth;
                        } else {
                            this.x = enemy.x - thisWidth;
                        }
                    } else {
                        if (y > 0) {
                            this.y = enemy.y + enemyHeight;
                        } else {
                            this.y = enemy.y - thisHeight;
                        }
                    }

                    enemy.chain(1);
                    this.chain(1);
                    return;
                }
            }

        },
        ontouchend() {
            if (this.phase !== "wait") return;
            if (this.scene.getCurrentPlayer() !== null) return;

            this.speedX = 0;
            this.speedY = 0;

            this.tl2.clear();

            this.point = this.createPoint();
            this.scene.addPoint(this.point);

            this.scene.setCurrentPlayer(this);

            this.phase = "touch";
        },
        onparenttouchstart(evt) {
            if (this.phase !== "touch") return;

            this.origX = this.x;
            this.origY = this.y;
            this.startX = evt.x;
            this.startY = evt.y;

            this.phase = "move";
        },
        onparenttouchmove(evt) {
            if (this.phase !== "move") return;

            this.moveX = evt.x;
            this.moveY = evt.y;

            this.x = (this.moveX - this.startX) / 2 + this.origX;
            this.y = (this.moveY - this.startY) / 2 + this.origY;
        },
        onparenttouchend(evt) {
            if (this.phase !== "move") return;

            this.speedX = (this.startX - evt.x) / 4;
            this.speedY = (this.startY - evt.y) / 4;

            this.point.delete();
            this.point = null;

            this.scene.setCurrentPlayer(null);

            this.phase = "wait";
        },
        onparenttouchenter() {
            // nothing
        },
        onparenttouchleave() {
            this.onparenttouchend({ x: this.moveX, y: this.moveY });
        }
    });

    const DsEnemy = Class.create(Sprite, {
        initialize(options) {
            Sprite.call(this, options.width, options.height);

            this.x = options.x;
            this.y = options.y;

            this.color = options.color;
            this.phase = "wait";
            this.time = options.time;
        }
    });

    const DsEnemyNormal = Class.create(DsEnemy, {
        initialize(options) {
            DsEnemy.call(this, Object.assign({ width: 48, height: 48 }, options));

            this.scaleX = 0.5;
            this.scaleY = 0.5;

            this.endX = options.endX || options.x;
            this.endY = options.endY || options.y;
            this.ease = options.ease || "LINEAR";

            this.score = 100;

            this.image = new Surface(48, 48);
            this.image.draw(core.assets[ds.images.color], -this.color * 48, 0);
            this.image.draw(core.assets[ds.images.enemy], 0, 0, 48, 48);

            this.tl
                .fadeIn(10).and().scaleTo(1, 10).and().moveTo(this.endX, this.endY, this.time - 60, enchant.Easing[this.ease])
                .scaleTo(0.8, 10)
                .delay(50)
                .then(() => this.attack());
        },
        createEffect(pattern) {
            return new DsEffect({
                width: this.width,
                height: this.height,
                x: this.x,
                y: this.y,
                scaleX: this.scaleX,
                scaleY: this.scaleY,
                image: this.image,
                pattern: pattern
            });
        },
        createCircle(chain) {
            return new DsCircle({
                x: this.x + this.width / 2 - 48,
                y: this.y + this.height / 2 - 48,
                color: this.color,
                chain: chain
            });
        },
        attack() {
            if (this.phase === "end") return;

            this.scene.setLife(-1);

            const effect = this.createEffect("attack");
            this.scene.addEffect(effect);

            this.scene.removeEnemy(this);

            this.phase = "end";
        },
        chain(chain) {
            if (this.phase === "end") return;

            this.scene.setScore(-this.score * ((1 + chain) / 2));

            const effect = this.createEffect("chain");
            this.scene.addEffect(effect);

            const circle = this.createCircle(chain);
            this.scene.addCircle(circle);

            this.scene.removeEnemy(this);

            this.phase = "end";
        }
    });

    const DsEffect = Class.create(Sprite, {
        initialize(options) {
            Sprite.call(this, options.width, options.height);

            this.x = options.x;
            this.y = options.y;
            this.scaleX = options.scaleX;
            this.scaleY = options.scaleY;
            this.image = options.image;

            switch (options.pattern) {
                case "delete":
                    this.tl
                        .scaleTo(1.4, 30).and().fadeOut(30)
                        .then(() => this.scene.removeEffect(this));
                    break;

                case "chain":
                    this.tl
                        .scaleTo(0.6, 15).and().rotateTo(180, 15).and().fadeTo(0.5, 15)
                        .scaleTo(1.4, 15).and().rotateTo(360, 15).and().fadeOut(15)
                        .then(() => this.scene.removeEffect(this));
                    break;

                case "attack":
                    this.tl
                        .moveBy(-10, -10, 2).and().scaleTo(1.2, 3)
                        .moveBy(15, 10, 2).and().scaleTo(0.4, 2)
                        .moveBy(-15, 10, 2).and().scaleTo(1.2, 3)
                        .moveBy(15, 5, 2).and().scaleTo(0.8, 2)
                        .moveBy(5, -10, 2).and().scaleTo(1.4, 3)
                        .moveBy(-5, -10, 2).and().scaleTo(0.8, 2)
                        .moveBy(-5, 10, 2).and().scaleTo(1, 3)
                        .scaleTo(2, 7).and().fadeOut(7)
                        .then(() => this.scene.removeEffect(this));
                    break;
            }
        }
    });

    const DsPoint = Class.create(Sprite, {
        initialize(options) {
            Sprite.call(this, 48, 48);

            this.x = options.x;
            this.y = options.y;
            this.scaleX = 0;
            this.scaleY = 0;
            this.image = core.assets[ds.images.point];

            this.tl
                .scaleTo(1, 10);
        },
        delete() {
            this.tl
                .clear()
                .scaleTo(1.4, 10).and().fadeOut(10)
                .then(() => this.scene.removePoint(this));
        }
    });

    const DsCircle = Class.create(Sprite, {
        initialize(options) {
            Sprite.call(this, 96, 96);

            this.x = options.x;
            this.y = options.y;
            this.scaleX = 0;
            this.scaleY = 0;
            this.image = core.assets[ds.images.circle];

            this.color = options.color;
            this.chain = options.chain + 1;

            this.tl
                .scaleTo(2, 30).and().fadeOut(30, enchant.Easing.SIN_EASEIN)
                .then(() => this.scene.removeCircle(this));
        },
        onenterframe() {
            for (const player of this.scene.players) {
                if (!this.within(player, this.width * this.scaleX / 2)) continue;
                if (this.color !== 7 && this.color !== player.color) continue;

                player.chain(this.chain);
            }

            for (const enemy of this.scene.enemies) {
                if (!this.within(enemy, this.width * this.scaleX / 2)) continue;
                if (this.color !== 7 && this.color !== enemy.color) continue;

                enemy.chain(this.chain);
            }
        }
    });

    const DsButton = Class.create(Sprite, {
        initialize(options) {
            Sprite.call(this, options.width, options.height);

            this.x = options.x;
            this.y = options.y;
            this.image = options.image;
            this.ontouchend = options.ontouchend;
        }
    });

    const DsLabel = Class.create(Group, {
        initialize(options) {
            Group.call(this);

            this.x = options.x;
            this.y = options.y;

            this.icon = new Sprite(24, 24);
            this.icon.image = options.image;
            this.addChild(this.icon);

            this.label = new SpriteLabel(24, core.assets[ds.images.font]);
            this.label.x = 24;
            this.label.y = 0;
            this.label.gap = -4;
            this.label.text = options.text;
            this.addChild(this.label);
        },
        text: {
            get() {
                return this.label.text;
            },
            set(text) {
                this.label.text = text;
            }
        }
    });

    const SpriteLabel = enchant.Class.create(enchant.Sprite, {
        initialize(size, font) {
            enchant.Sprite.call(this, 0, 0);

            this._size = size;
            this._fontSize = font.width / size;
            this._row = 0;
            this._gap = 0;
            this._font = font;
            this._text = "";
        },
        drawText() {
            const size = this._size + this._gap;
            const row = this._row === 0 ? this._text.length : this._row;
            const column = (this._text.length === 0 || this._row === 0) ? 1 : Math.ceil(this._text.length / this._row);

            this.width = Math.min(row === 0 ? 0 : this._size + (row - 1) * size, enchant.Game.instance.width);
            this.height = Math.min(column === 0 ? 0 : this._size + (column - 1) * size, enchant.Game.instance.height);

            if (!this.image ||
                this.width > this.image.width || this.height > this.image.height ||
                this.width < this.image.width - 256 || this.height < this.image.height - 256) {
                this.image = new enchant.Surface(this.width, this.height);
            }

            this.image.context.clearRect(0, 0, this.image.width, this.image.height);

            for (let i = 0; i < this._text.length; i++) {
                const charCode = this._text.charCodeAt(i);
                const charPos = (charCode >= 32 && charCode <= 127) ? charCode - 32 : 0;

                this.image.draw(
                    this._font,
                    (charPos % this._fontSize) * this._size,
                    Math.floor(charPos / this._fontSize) * this._size,
                    this._size,
                    this._size,
                    (this._row === 0 ? i : (i % this._row)) * size,
                    (this._row === 0 ? 0 : Math.floor(i / this._row)) * size,
                    this._size,
                    this._size
                );
            }
        },
        text: {
            get() {
                return this._text;
            },
            set(text) {
                if (text.toString() === this._text) return;

                this._text = text.toString();
                this.drawText();
            }
        },
        row: {
            get() {
                return this._row;
            },
            set(row) {
                if (row < 0) return;

                this._row = row;
                this.drawText();
            }
        },
        gap: {
            get() {
                return this._gap;
            },
            set(gap) {
                this._gap = gap;
                this.drawText();
            }
        }
    });

    window.addEventListener("load", () => {
        function isFullscreenEnabled() {
            return document.fullscreenEnabled ||
                document.webkitFullscreenEnabled ||
                document.mozFullScreenEnabled ||
                document.msFullscreenEnabled ||
                false;
        }

        function getRequestFullscreenName(elem) {
            return [
                "requestFullscreen",
                "webkitRequestFullScreen",
                "mozRequestFullScreen",
                "msRequestFullscreen"
            ].find(name => {
                if (!(name in elem)) return false;

                return true;
            });
        }

        function getFullScreenElement() {
            return document.fullscreenElement ||
                document.webkitFullscreenElement ||
                document.mozFullScreenElement ||
                document.msFullscreenElement ||
                null;
        }

        const width = 640;
        const height = 360;

        // Game
        core = new Core(640, 360);

        core.fps = 60;
        core.preload(Object.keys(ds.images).map(key => ds.images[key]));

        core.addEventListener("load", () => {
            const dsMain = new DsMain(ds.stages[0]);

            core.pushScene(dsMain);
        });

        core.start();

        // DOM
        const wrapper = document.querySelector("#enchant-stage");

        function resize() {
            core.scale = window.innerWidth * height > window.innerHeight * width ? window.innerHeight / height : window.innerWidth / width;
        }

        resize();

        window.addEventListener("resize", resize);

        if (/iPhone|iPad|Android|Mobile/.test(navigator.userAgent)) {
            if (isFullscreenEnabled()) {
                wrapper.addEventListener("touchstart", () => {
                    if (getFullScreenElement() !== null) return;

                    wrapper[getRequestFullscreenName(wrapper)]();
                });
            }
        }
    });
}
