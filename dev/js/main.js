/* global enchant, Core, Class, Scene, Sprite, Surface, ds*/

"use strict";

{
    enchant();

    let core;

    const DsMain = Class.create(Scene, {
        initialize() {
            Scene.call(this);

            this.phase = "wait";
            this.currentBlock = null;
            this.currentColor = 0;
            this.currentTime = 300;
            this.currentEase = "LINEAR";
            this.currentState = -1;
            this.table = [];
            this.blocks = [];

            this.background = new Sprite(this.width, this.height);
            this.background.image = core.assets[ds.images.bg];
            this.addChild(this.background);

            const color = document.querySelector("#color");
            color.value = this.currentColor;
            color.addEventListener("change", () => {
                this.currentColor = Number.parseInt(color.value, 10);
            });

            const time = document.querySelector("#time");
            time.value = this.currentTime / 60;
            time.addEventListener("change", () => {
                this.currentTime = Number.parseFloat(time.value) * 60;
            });

            const ease = document.querySelector("#ease");
            ease.value = this.currentEase;
            ease.addEventListener("change", () => {
                this.currentEase = ease.value;
            });

            const output = document.querySelector("#output");
            output.value = "";
            output.addEventListener("change", () => {
                this.loadTable();
            });

            const table = document.querySelector("#table");
            table.addEventListener("click", () => {
                if (table.selectedIndex === -1) return;

                this.load(Number.parseInt(table[table.selectedIndex].value, 10));
            });

            const add = document.querySelector("#add");
            add.addEventListener("click", () => {
                this.add(Number.parseInt(prompt("seconds"), 10) * 60);
            });

            const remove = document.querySelector("#remove");
            remove.addEventListener("click", () => {
                if (table.selectedIndex === -1) return;

                this.remove(Number.parseInt(table[table.selectedIndex].value, 10));
            });

            const change = document.querySelector("#change");
            change.addEventListener("click", () => {
                if (table.selectedIndex === -1) return;

                this.change(Number.parseInt(table[table.selectedIndex].value, 10), Number.parseInt(prompt("seconds"), 10) * 60);
            });
        },
        add(stateIndex) {
            if (this.table.hasOwnProperty(stateIndex)) return;

            const elemTable = document.querySelector("#table");

            this.table[stateIndex] = {
                players: [],
                enemies: []
            };

            Array.from(elemTable.children).forEach(option => {
                elemTable.removeChild(option);
            });

            this.table.forEach((value, index) => {
                const option = document.createElement("option");
                option.setAttribute("value", index);
                option.textContent = index / 60;
                elemTable.appendChild(option);
            });

            elemTable.selectedIndex = this.table.filter(Object).indexOf(this.table[stateIndex]);
            this.load(stateIndex);
        },
        remove(stateIndex) {
            if (!this.table.hasOwnProperty(stateIndex)) return;

            const elemTable = document.querySelector("#table");

            const filterdTable = this.table.filter(Object);
            const filterdIndex = filterdTable.indexOf(this.table[stateIndex]);

            elemTable.selectedIndex = filterdIndex - 1;
            this.load(this.table.indexOf(filterdTable[filterdIndex - 1]));

            delete this.table[stateIndex];
            elemTable.removeChild(elemTable.children[filterdIndex]);

            this.saveTable();
        },
        change(oldIndex, newIndex) {
            if (!this.table.hasOwnProperty(oldIndex)) return;
            if (this.table.hasOwnProperty(newIndex)) return;

            const elemTable = document.querySelector("#table");

            this.table[newIndex] = this.table[oldIndex];
            delete this.table[oldIndex];

            Array.from(elemTable.children).forEach(option => {
                elemTable.removeChild(option);
            });

            this.table.forEach((value, index) => {
                const option = document.createElement("option");
                option.setAttribute("value", index);
                option.textContent = index / 60;
                elemTable.appendChild(option);
            });

            elemTable.selectedIndex = this.table.filter(Object).indexOf(this.table[newIndex]);
            this.load(newIndex);
            this.saveTable();
        },
        loadTable() {
            const elemOutput = document.querySelector("#output");
            const elemTable = document.querySelector("#table");
            let table;

            try {
                table = JSON.parse(elemOutput.value);
            } catch (err) {
                console.error(err);
                return;
            }

            this.table.length = 0;

            Array.from(elemTable.children).forEach(option => {
                elemTable.removeChild(option);
            });

            for (const key of Object.keys(table)) {
                this.table[Number.parseInt(key, 10)] = table[key];
            }

            this.table.forEach((value, index) => {
                const option = document.createElement("option");
                option.setAttribute("value", index);
                option.textContent = index / 60;
                elemTable.appendChild(option);
            });

            elemTable.selectedIndex = 0;
            this.load(Number.parseInt(elemTable.options[0].value, 10));
        },
        saveTable() {
            const elemOutput = document.querySelector("#output");

            elemOutput.value = "{\n" +
                this.table.map((state, index) => {
                    return `    "${index}": {\n` +
                        `        \"players\": [${state.players.length === 0 ? "" : `\n${state.players.reduce((players, player) => `${players === "" ? "" : `${players},\n`}            ${JSON.stringify(player).replace(/,/g, ", ").replace(/:/g, ": ")}`, "")}\n        `}],\n` +
                        `        \"enemies\": [${state.enemies.length === 0 ? "" : `\n${state.enemies.reduce((enemies, enemy) => `${enemies === "" ? "" : `${enemies},\n`}            ${JSON.stringify(enemy).replace(/,/g, ", ").replace(/:/g, ": ")}`, "")}\n        `}]\n` +
                        "    }";
                }).filter(String).join(",\n") +
                "\n}";
        },
        load(stateIndex) {
            this.currentState = stateIndex;

            this.blocks.slice().forEach(block => this.removeBlock(block));
            this.currentBlock = null;

            if (stateIndex === -1) return;

            const state = this.table[stateIndex];

            state.players.forEach(player => {
                const block = this.createBlock({
                    x: player.x + 88,
                    y: player.y + 84,
                    color: player.color,
                    time: player.time,
                    ease: player.ease || "LINEAR",
                    type: "player"
                });
                block.end.x = player.endX === void 0 ? block.end.x : player.endX + 88;
                block.end.y = player.endY === void 0 ? block.end.y : player.endY + 84;
                this.addBlock(block);
            });

            state.enemies.forEach(player => {
                const block = this.createBlock({
                    x: player.x + 88,
                    y: player.y + 84,
                    color: player.color,
                    time: player.time,
                    ease: player.ease || "LINEAR",
                    type: "enemy"
                });
                block.end.x = player.endX === void 0 ? block.end.x : player.endX + 88;
                block.end.y = player.endY === void 0 ? block.end.y : player.endY + 84;
                this.addBlock(block);
            });
        },
        save() {
            if (this.currentState === -1) return;

            const output = {};

            output.players = this.blocks.filter(block => block.type === "player").map(block => {
                const player = {};

                player.color = block.color;
                player.time = block.time;
                player.x = block.x - 88;
                player.y = block.y - 84;

                if (block.x !== block.end.x || block.y !== block.end.y) {
                    player.endX = block.end.x - 88;
                    player.endY = block.end.y - 84;
                }

                if (block.ease !== "LINEAR") {
                    player.ease = block.ease;
                }

                return player;
            });

            output.enemies = this.blocks.filter(block => block.type === "enemy").map(block => {
                const enemy = {};

                enemy.color = block.color;
                enemy.time = block.time;
                enemy.x = block.x - 88;
                enemy.y = block.y - 84;

                if (block.x !== block.end.x || block.y !== block.end.y) {
                    enemy.endX = block.end.x - 88;
                    enemy.endY = block.end.y - 84;
                }

                if (block.ease !== "LINEAR") {
                    enemy.ease = block.ease;
                }

                return enemy;
            });

            this.table[this.currentState] = output;
        },
        createBlock(options) {
            const block = new DsBlock(Object.assign({ opacity: 1 }, options));
            block.end = new DsBlock(Object.assign({ opacity: 0.5 }, options));

            return block;
        },
        addBlock(block) {
            this.addChild(block);
            this.addChild(block.end);
            this.blocks.push(block);
        },
        removeBlock(block) {
            this.removeChild(block);
            this.removeChild(block.end);
            this.blocks.splice(this.blocks.indexOf(block), 1);
        },
        ontouchstart(evt) {
            switch (this.phase) {
                case "wait": {
                    const currentBlock = this.blocks.find(block => {
                        if (block.x !== Math.floor(evt.x / 48) * 48) return false;
                        if (block.y !== Math.floor(evt.y / 48) * 48) return false;

                        return true;
                    });

                    if (currentBlock === void 0) {
                        this.currentBlock = null;
                    } else {
                        this.currentBlock = currentBlock;
                        this.currentBlock.end.x = this.currentBlock.x;
                        this.currentBlock.end.y = this.currentBlock.y;

                        this.phase = "move";
                    }
                    break;
                }
            }
        },
        ontouchmove(evt) {
            switch (this.phase) {
                case "move": {
                    this.currentBlock.end.x = Math.floor(evt.x / 48) * 48;
                    this.currentBlock.end.y = Math.floor(evt.y / 48) * 48;
                    break;
                }
            }
        },
        ontouchend(evt) {
            switch (this.phase) {
                case "wait": {
                    const currentBlock = this.blocks.find(block => {
                        if (block.x !== Math.floor(evt.x / 48) * 48) return false;
                        if (block.y !== Math.floor(evt.y / 48) * 48) return false;

                        return true;
                    });

                    if (currentBlock !== void 0) break;

                    this.currentBlock = this.createBlock({
                        x: Math.floor(evt.x / 48) * 48,
                        y: Math.floor(evt.y / 48) * 48,
                        color: this.currentColor,
                        time: this.currentTime,
                        ease: this.currentEase,
                        type: "player"
                    });
                    this.addBlock(this.currentBlock);
                    break;
                }
                case "move": {
                    if (this.currentBlock.x === this.currentBlock.end.x && this.currentBlock.y === this.currentBlock.end.y) {
                        if (this.currentBlock.type === "player") {
                            this.removeBlock(this.currentBlock);

                            this.currentBlock = this.createBlock({
                                x: Math.floor(evt.x / 48) * 48,
                                y: Math.floor(evt.y / 48) * 48,
                                color: this.currentColor,
                                time: this.currentTime,
                                ease: this.currentEase,
                                type: "enemy"
                            });
                            this.addBlock(this.currentBlock);
                        } else {
                            this.removeBlock(this.currentBlock);
                        }
                    }

                    this.phase = "wait";
                    break;
                }
            }

            this.save();
            this.saveTable();
        }
    });

    const DsBlock = Class.create(Sprite, {
        initialize(options) {
            Sprite.call(this, 48, 48);

            this.x = options.x;
            this.y = options.y;
            this.opacity = options.opacity;

            this.color = options.color;
            this.time = options.time;
            this.ease = options.ease;
            this.type = options.type;

            this.image = new Surface(48, 48);
            this.image.draw(core.assets[ds.images.color], -this.color * 48, 0);
            this.image.draw(core.assets[ds.images[options.type]], 0, 0, 48, 48);
            this.image.context.fillStyle = "rgba(0,0,0,0.5)";
            this.image.context.fillRect(4,10,40,30);
            this.image.context.font = "10px";
            this.image.context.fillStyle = "white";
            this.image.context.fillText(`${this.time / 60} sec`, 4, 18);
            this.image.context.fillText(`${this.ease.split("_")[0]}`, 4, 28);
            this.image.context.fillText(`${this.ease.split("_")[1] || ""}`, 4, 38, 40);
        }
    });

    window.addEventListener("load", () => {
        core = new Core(816, 528);

        Object.keys(ds.images).forEach(key => {
            ds.images[key] = `../${ds.images[key]}`;
        });

        ds.images = Object.assign({
            bg: "img/bg.png"
        }, ds.images);

        core.fps = 60;
        core.preload(Object.keys(ds.images).map(key => ds.images[key]));

        core.addEventListener("load", () => {
            const dsMain = new DsMain();
            core.pushScene(dsMain);
        });

        core.start();
    });
}
