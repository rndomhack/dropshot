let ds = {
    "stages": [
        {
            "time": 1800,
            "life": 10,
            "table":
            {
                "0": {
                    "players": [
                        {"color": 0, "time": 360, "x": 104, "y": 252},
                        {"color": 0, "time": 360, "x": 488, "y": 252}
                    ],
                    "enemies": [
                        {"color": 0, "time": 360, "x": 152, "y": 60},
                        {"color": 0, "time": 360, "x": 440, "y": 60}
                    ]
                },
                "120": {
                    "players": [
                        {"color": 2, "time": 240, "x": 536, "y": 156},
                        {"color": 2, "time": 240, "x": 56, "y": 156},
                        {"color": 2, "time": 240, "x": 296, "y": 60},
                        {"color": 2, "time": 240, "x": 296, "y": 252}
                    ],
                    "enemies": []
                },
                "360": {
                    "players": [
                        {"color": 1, "time": 360, "x": 152, "y": 252},
                        {"color": 0, "time": 360, "x": 440, "y": 252}
                    ],
                    "enemies": [
                        {"color": 1, "time": 360, "x": 536, "y": 60, "endX": 392, "endY": 60},
                        {"color": 0, "time": 360, "x": 56, "y": 60, "endX": 200, "endY": 60}
                    ]
                },
                "480": {
                    "players": [
                        {"color": 2, "time": 240, "x": 584, "y": 12, "endX": 584, "endY": 156},
                        {"color": 2, "time": 240, "x": 584, "y": 300, "endX": 296, "endY": 300},
                        {"color": 2, "time": 240, "x": 8, "y": 300, "endX": 8, "endY": 156},
                        {"color": 2, "time": 240, "x": 8, "y": 12, "endX": 296, "endY": 12}
                    ],
                    "enemies": []
                },
                "720": {
                    "players": [
                        {"color": 0, "time": 360, "x": 536, "y": 156},
                        {"color": 1, "time": 360, "x": 56, "y": 156}
                    ],
                    "enemies": [
                        {"color": 1, "time": 360, "x": 536, "y": 12, "endX": 56, "endY": 300, "ease": "QUAD_EASEINOUT"},
                        {"color": 0, "time": 360, "x": 56, "y": 12, "endX": 536, "endY": 300, "ease": "QUAD_EASEINOUT"}
                    ]
                },
                "840": {
                    "players": [
                        {"color": 3, "time": 240, "x": 488, "y": 300, "endX": 392, "endY": 204},
                        {"color": 3, "time": 240, "x": 104, "y": 12, "endX": 200, "endY": 108},
                        {"color": 2, "time": 240, "x": 488, "y": 12, "endX": 392, "endY": 108},
                        {"color": 2, "time": 240, "x": 104, "y": 300, "endX": 200, "endY": 204}
                    ],
                    "enemies": []
                },
                "1080": {
                    "players": [
                        {"color": 0, "time": 360, "x": 296, "y": 252}
                    ],
                    "enemies": [
                        {"color": 0, "time": 360, "x": 56, "y": 12},
                        {"color": 0, "time": 360, "x": 104, "y": 60},
                        {"color": 0, "time": 360, "x": 152, "y": 12},
                        {"color": 0, "time": 360, "x": 200, "y": 60},
                        {"color": 0, "time": 360, "x": 248, "y": 12},
                        {"color": 0, "time": 360, "x": 296, "y": 60},
                        {"color": 0, "time": 360, "x": 344, "y": 12},
                        {"color": 0, "time": 360, "x": 392, "y": 60},
                        {"color": 0, "time": 360, "x": 440, "y": 12},
                        {"color": 0, "time": 360, "x": 488, "y": 60},
                        {"color": 0, "time": 360, "x": 536, "y": 12}
                    ]
                },
                "1200": {
                    "players": [
                        {"color": 2, "time": 240, "x": 392, "y": 204},
                        {"color": 2, "time": 240, "x": 200, "y": 204},
                        {"color": 2, "time": 240, "x": 584, "y": 252},
                        {"color": 2, "time": 240, "x": 536, "y": 300},
                        {"color": 2, "time": 240, "x": 8, "y": 252},
                        {"color": 2, "time": 240, "x": 56, "y": 300}
                    ],
                    "enemies": []
                },
                "1440": {
                    "players": [
                        {"color": 0, "time": 360, "x": 296, "y": 300},
                        {"color": 7, "time": 360, "x": 296, "y": 156}
                    ],
                    "enemies": [
                        {"color": 2, "time": 360, "x": 584, "y": 12, "endX": 248, "endY": 204, "ease": "BACK_EASEINOUT"},
                        {"color": 3, "time": 360, "x": 8, "y": 300, "endX": 344, "endY": 108, "ease": "BACK_EASEINOUT"},
                        {"color": 1, "time": 360, "x": 8, "y": 12, "endX": 344, "endY": 204, "ease": "BACK_EASEINOUT"},
                        {"color": 4, "time": 360, "x": 584, "y": 300, "endX": 248, "endY": 108, "ease": "BACK_EASEINOUT"}
                    ]
                },
                "1560": {
                    "players": [
                        {"color": 6, "time": 240, "x": 680, "y": 396, "endX": 104, "endY": 252},
                        {"color": 6, "time": 240, "x": -88, "y": 396, "endX": 104, "endY": 60},
                        {"color": 6, "time": 240, "x": -88, "y": -84, "endX": 488, "endY": 60},
                        {"color": 6, "time": 240, "x": 680, "y": -84, "endX": 488, "endY": 252}
                    ],
                    "enemies": []
                }
            }
        }
    ],
    "images": {
        "font": "img/font.png",
        "player": "img/player.png",
        "enemy": "img/enemy.png",
        "color": "img/color.png",
        "point": "img/point.png",
        "circle": "img/circle.png",
        "icon_pause": "img/icon_pause.png",
        "icon_life": "img/icon_life.png",
        "icon_time": "img/icon_time.png",
        "icon_score": "img/icon_score.png"
    }
};
